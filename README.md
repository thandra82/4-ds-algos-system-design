# Algorithms and System Design Courses

## Algorithms and DS
-  [ Mohammad Bari ](https://www.udemy.com/course/datastructurescncpp/) - My favorite in C - practice in JAVA
-  [ Zero to mastery ](https://www.udemy.com/course/master-the-coding-interview-data-structures-algorithms/) - Zero to mastery


## System Design
-  [ Alex xu ](https://bytebytego.com/courses/system-design-interview) - Read
-  [ Gaurav Sen - Interview ready ](https://interviewready.io/learn/system-design-course) - Videos
-  [ Zero to Software Architect ](https://enrolled.zerotosoftwarearchitect.com/courses/enrolled/1016183) - Read


