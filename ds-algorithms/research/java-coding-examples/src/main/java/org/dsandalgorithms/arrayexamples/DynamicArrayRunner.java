package org.dsandalgorithms.arrayexamples;

public class DynamicArrayRunner {
    public static void main(String[] args) throws Exception {


        DynamicArray<String> array = new DynamicArray<>(5);

        array.addElement("Srini");
        array.addElement("Priya");
        array.addElement("Shreyus");

        array.display();

        array.addElementAtIndex("lakshmi", 2);

        array.display();

        array.addElement("Sachin");


        array.display();


        array.deleteElement(3);

        array.display();

        int elementIndex = array.linearSearch("Priya");

        System.out.println("element found at index::: " + elementIndex);
    }
}
