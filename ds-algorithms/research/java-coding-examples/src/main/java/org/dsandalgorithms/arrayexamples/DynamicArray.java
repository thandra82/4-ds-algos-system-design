package org.dsandalgorithms.arrayexamples;

public class DynamicArray<T> {

    private int size = 0;
    private int length = 0;
    private final T[] elements;

    @SuppressWarnings({"unchecked"})
    public DynamicArray(int size) {
        this.size = size;
        this.elements = (T[]) new Object[this.size];
    }

    public void addElement(T element) throws IndexOutOfBoundsException {
        if (this.length < this.size) {
            this.elements[this.length++] = element;
        } else {
            throw new IndexOutOfBoundsException("Can not insert");
        }
    }

    public void addElementAtIndex(T element, int index) throws Exception {
        if (index < 0 && index > this.size) {
            throw new Exception("Invalid index passed");
        }
        for (int i = this.length; i > index; i--) {
            this.elements[i] = this.elements[i - 1];
        }
        this.elements[index] = element;
        this.length++;
    }

    public T deleteElement(int index) {
        T deletedElement = null;
        if (index >= 0 && index < this.length) {
            deletedElement = this.elements[index];
            for (int i = index; i < this.length - 1; i++) {
                this.elements[index] = this.elements[index + 1];
            }
            /* make the last element null */
            this.elements[this.length - 1] = null;
        }

        return deletedElement;
    }

    public int linearSearch(T element) {
        int elementIndex = 0;
        for (int i = 0; i < this.length; i++) {
            if (element.equals(this.elements[i])) {
                elementIndex = i;
            }
        }

        return elementIndex;
    }

    public void display() {
        System.out.println("Start of Display *******************");
        for (T element : elements) {
            System.out.println(element);
        }
        System.out.println("End of Display *******************");
    }


}
